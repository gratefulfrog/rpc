# RPC

This is my version of the [ONC+ Developers Guide RPC Tutorial](https://docs.oracle.com/cd/E19683-01/816-1435/rpcgenpguide-21470/index.html).

To use it follow the instructions in instructions.txt file.

Some useful reference materiel:
* [SunRPC and XML-RPC over GnuTLS (SSL, TLS) and ssh](https://people.redhat.com/rjones/secure_rpc/)
* [Sun Remote Procedure Call Mechanism](https://web.cs.wpi.edu/~rek/DCS/D04/SunRPC.html)
* [A Lower-Level View of Remote Procedure Calls](https://xerocrypt.wordpress.com/tag/rpcgen/)   
* Overview of Python RPC options [What is the current choice (2011) for doing RPC in python?](https://stackoverflow.com/questions/1879971/what-is-the-current-choice-for-doing-rpc-in-python)
* python [Pyro](https://pyro4.readthedocs.io/en/stable/) full good documentation, mature python RPC library
* python [RPyC](https://rpyc.readthedocs.io/en/latest/) full decoumentation, examples are not so easy to follow, but seems to have more features than Pyro
    * [Distributing computing with RPyC](https://www.ibm.com/developerworks/linux/library/l-rpyc/) 
* multi-language RPC, use [JSON-RPC](https://www.jsonrpc.org/) and perhaps this python module [json-rpc 1.11.1](https://pypi.org/project/json-rpc/) 