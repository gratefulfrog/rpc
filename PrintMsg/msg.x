/*
* First, determine the data types of all procedure-calling arguments 
* and the  resulting argument. The calling argument of printmessage() 
* is a string, and the result is an integer. 
* You can write a protocol specification in the RPC language that 
* describes the remote version of printmessage(). 
* The RPC language source code for such a specification is:
*/

/* msg.x: Remote msg printing protocol */
 program MESSAGEPROG {
     version PRINTMESSAGEVERS {
        int PRINTMESSAGE(string) = 1;
 	 } = 1;
} = 0x20000001;

/*
 * Remote procedures are always declared as part of remote programs. The 
 * previous code declares an entire remote program that contains the 
 * single procedure PRINTMESSAGE. In this example, the PRINTMESSAGE 
 * procedure is declared to be procedure 1, in version 1 of the remote 
 * program MESSAGEPROG, with the program number 0x20000001. See 
 * Appendix B, RPC Protocol and Language Specification for guidance on 
 * choosing program numbers.

 * Version numbers are incremented when functionality is changed in the 
 * remote program. Existing procedures can be changed or new ones can 
 * be added. More than one version of a remote program can be defined 
 * and a version can have more than one procedure defined. 

 * Note that the program and procedure names are declared with all 
 * capital letters. 

 * Note also that the argument type is string and not char  * as it would 
 * be in C. This is because a char  * in C is ambiguous. char usually 
 * means an array of characters, but it could also represent a pointer 
 * to a single character. In the RPC language, a null-terminated array 
 * of char is called a string. 
 */
