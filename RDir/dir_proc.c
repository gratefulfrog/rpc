/*
 * dir_proc.c: remote readdir
 * implementation
 */

#include <dirent.h>
#include "dir.h"        /* Created by rpcgen */

#include <errno.h>
/*#define EAGAIN      11   Try again */

/*extern int errno; */
/* extern char *malloc(); */
extern void *malloc();
extern char *strdup();

/*  CORRECTION !
in msg_proc.c change function
readdir_res *readdir_1(
readdir_res *readdir_1_svc(
this is the difference in Unix rpcgen and linux rpcgen
*/

readdir_res *readdir_1_svc(nametype *dirname, struct svc_req *req){
  DIR *dirp;
  struct dirent *d;
  namelist nl;
  namelist *nlp;
  static readdir_res res; /* must be static! */
  
  /* Open directory */
  dirp = opendir(*dirname);
  if (dirp == NULL) {
    res.merrno = errno;
    return (&res);
  }
  /* Free previous result */
  xdr_free((xdrproc_t)xdr_readdir_res, (char*)&res);
  /*
   * Collect directory entries.
   * Memory allocated here is free by
   * xdr_free the next time readdir_1
   * is called
   */
  nlp = &res.readdir_res_u.list;
  while (d = readdir(dirp)) {
    nl = *nlp = (namenode *)  malloc(sizeof(namenode));
    if (nl ==  NULL) {
      res.merrno = EAGAIN;
      closedir(dirp);
      return(&res);
    }
    nl->name = strdup(d->d_name);
    nlp = &nl->next;
  }
  *nlp = NULL;
  /* Return the result */
  res.merrno = 0;
  closedir(dirp);
  return (&res);
}
